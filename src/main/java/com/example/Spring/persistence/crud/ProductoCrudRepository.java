package com.example.Spring.persistence.crud;

import com.example.Spring.persistence.entity.Producto;
//import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.CrudRepository;

import java.util.List;
import java.util.Optional;

public interface ProductoCrudRepository extends CrudRepository <Producto, Integer> {

    //CONSULTA REALIZADA DE MANERA NATIVA EN SQL
    //@Query(value = "SELECT * FROM productos WHERE id_categoria = ?", nativeQuery = true)

    List<Producto> findAllByIdCategoriaOrderByNombreAsc(int idCategoria);

    Optional<List<Producto>> findAllByCantidadStockLessThanAndEstado(int cantidadStock, boolean estado);
}
