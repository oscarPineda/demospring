package com.example.Spring.persistence;

import com.example.Spring.persistence.crud.ProductoCrudRepository;
import com.example.Spring.persistence.entity.Producto;

import java.util.List;
import java.util.Optional;

public class ProductoRepository {
    private ProductoCrudRepository productoCrudRepository;

    // OBTENER TODOS LOS PRODUCTOS
    public List<Producto> getAll(){
        return (List<Producto>) productoCrudRepository.findAll();
    }

    // OBTENER PRODUCTO POR CATEGORIA
    public List<Producto>getByCategoria(int idCategoria) {
        return productoCrudRepository.findAllByIdCategoriaOrderByNombreAsc(idCategoria);
    }

    // OBTENER PRODUCTOS ESCASOS
    public Optional<List<Producto>> getEscasos(int cantidad){
        return productoCrudRepository.findAllByCantidadStockLessThanAndEstado(cantidad, true);
    }

    // OBTENER PRODUCTO
    public Optional<Producto> getProducto (int idProducto){
        return productoCrudRepository.findById(idProducto);
    }

    public Producto save(Producto producto){
        return productoCrudRepository.save(producto);
    }
}
